package model;

public class Order {

	int id;
	int clientId;
	String comments;
	double total;

	public Order(int id, int clientId, String comments, double total) {
		this.id=id;
		this.clientId=clientId;
		this.comments=comments;
		this.total=total;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", clientId=" + clientId + ", comments=" + comments + ", total=" + total + "]";
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
