package model;

public class OrderDetail {

	int orderId;
	int productId;
    int quantity;
    double unitPrice;
    
    public OrderDetail() {
    	
    }
	public OrderDetail( int orderId, int productId, int quantity, double unitPrice) {
		this.orderId= orderId;
		this.productId= productId;
		this.quantity= quantity;
		this.unitPrice= unitPrice;
		
	}

	@Override
	public String toString() {
		return "OrderDetail [orderId=" + orderId + ", productId=" + productId + ", quantity=" + quantity
				+ ", unitPrice=" + unitPrice + "]";
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

}
