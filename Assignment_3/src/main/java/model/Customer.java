package model;

public class Customer {

	int id;
	String name;
	String address;
	String email;
	String phoneNr;
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(int id2, String name2, String address2, String email2, String phone_nr) {
		id=id2;
		name=name2;
		address=address2;
		email=email2;
		phoneNr=phone_nr; // I let it like this bc phone_nr appears in db
	}
	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", address=" + address + ", email=" + email + ", phoneNr="
				+ phoneNr + "]";
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNr() {
		return phoneNr;
	}

	public void setPhoneNr(String phoneNr) {
		this.phoneNr = phoneNr;
	}
}
