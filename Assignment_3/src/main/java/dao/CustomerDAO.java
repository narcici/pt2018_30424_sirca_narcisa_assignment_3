package dao;
// for now here I have findById, findByPhoneNumber, findAll, insert, and delete methods

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
/**
 * CustomerDAO class which implements the queries in order to perform insert, delete, update, findAll, findById, findByPhonenumber
 * operations on the database
 */
public class CustomerDAO {

	protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (id,name,address,email,phone_nr)"
			+ " VALUES (?,?,?,?,?)";
	private final static String findIdStatementString = "SELECT * FROM client where id= ?";

	private final static String findAllStatementString = "SELECT * FROM client ";
	private final static String deleteStatementString = "DELETE FROM client where  id=?";
	private static final String updateStatementString = "UPDATE client SET name=?, address=?, email=?, phone_nr=? WHERE id=?";

	public static int insert(Customer customer) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, customer.getId());
			insertStatement.setString(2, customer.getName());
			insertStatement.setString(3, customer.getAddress());
			insertStatement.setString(4, customer.getEmail());
			insertStatement.setString(5, customer.getPhoneNr());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
			//insertedId = rs.getInt(1);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static Customer findById(int customerId) {
		Customer toReturn = null;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findIdStatementString);
			findStatement.setInt(1, customerId); // id is pk/ an unique value
			rs = findStatement.executeQuery();
			rs.next();
			
			int id=rs.getInt("id");
			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			String phone_nr = rs.getString("phone_nr");
			toReturn = new Customer(id, name, address, email,phone_nr);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static ArrayList<Customer> findAll() {
		ArrayList<Customer> toReturn = new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();			
		Statement findStatement = null;;
		ResultSet rs = null;
		try {
			findStatement  = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery(findAllStatementString);
			while(rs.next()) {
				
			int id=rs.getInt("id");
			String name = rs.getString("name");
			String address = rs.getString("address");
			String email = rs.getString("email");
			String phone_nr = rs.getString("phone_nr");
			//Customer one = new Customer(id, name, address, email,phone_nr);
			toReturn.add(new Customer(id, name, address, email,phone_nr));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public void delete(int customerId) { // deleting method
		//Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, customerId); // phone number is an unique value
			deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
	public static void update(int id, String name, String address, String email, String phoneNr) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = dbConnection.prepareStatement(updateStatementString);
		updateStatement.setInt(5, id);
		//if(name.equals("") != true)	
		updateStatement.setString(1, name);
		//if(address.equals("") != true)	
		updateStatement.setString(2, address);
		//if(email.equals("") != true)	
		updateStatement.setString(3, email);
		//if(phoneNr.equals("") != true)	
		updateStatement.setString(4, phoneNr);
		updateStatement.executeUpdate();
		
	}
	
}
