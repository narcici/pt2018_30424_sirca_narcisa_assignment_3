package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;
/**
 * ProductDAO class which implements the queries in order to perform insert, delete, findAll, findById, orderByPrice, findByName
 * operations on the order_detail table from the database
 */
public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO product (id,name,price,quantity)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM product where name= ?";
	private final static String findAllStatementString = "SELECT * FROM product";
	private static final String updateStatementString = "UPDATE product SET name=?, price=?, quantity=? WHERE id=?";
	private final static String findIDStatementString = "SELECT * FROM product where id= ?";
	private final static String deleteStatementString = "DELETE FROM product where  id=?";
	private final static String orderAscStatementString = "SELECT * FROM product ORDER BY price LIMIT 5";

	public static ArrayList<Product> orderByPrice() {
		ArrayList<Product> toReturn = new ArrayList<Product>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(orderAscStatementString);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
			int id=rs.getInt("id");
			String name = rs.getString("name");
			double price = rs.getDouble("price");
			int quantity= rs.getInt("quantity");
			Product one = new Product(id, name, price, quantity);
			toReturn.add(one);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:orderByPrice " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Product findByName(String productName) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setNString(1, productName); // search for the name of a product
			rs = findStatement.executeQuery();
			rs.next();
			
			int id=rs.getInt("id");
			String name = rs.getString("name");
			double price = rs.getDouble("price");
			int quantity= rs.getInt("quantity");
			toReturn = new Product(id, name, price, quantity);
			} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static Product findById(int productId) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findIDStatementString);
			findStatement.setInt(1, productId); // search for the name of a product
			rs = findStatement.executeQuery();
			rs.next();
			
			int id=rs.getInt("id");
			String name = rs.getString("name");
			double price = rs.getDouble("price");
			int quantity= rs.getInt("quantity");
			toReturn = new Product(id, name, price, quantity);
			} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static ArrayList<Product> findAll() {
		ArrayList<Product> toReturn = new ArrayList<Product>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery();
			while(rs.next())
			{
			int id=rs.getInt("id");
			String name = rs.getString("name");
			double price = rs.getDouble("price");
			int quantity= rs.getInt("quantity");
			Product one = new Product(id, name, price, quantity);
			toReturn.add(one);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	public static int insert(Product customer) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, customer.getId());
			insertStatement.setString(2, customer.getName());
			insertStatement.setDouble(3, customer.getPrice());
			insertStatement.setInt(4, customer.getQuantity());
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public void delete(int idProduct) { // deleting method
		//Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, idProduct); // phone number is an unique value
			deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
 public static void update(int id, String name, double price, int quantity) throws SQLException {
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = dbConnection.prepareStatement(updateStatementString);
				
			updateStatement.setString(1, name);
			updateStatement.setDouble(2, price);
			updateStatement.setInt(3, quantity);
			updateStatement.setInt(4, id);
		updateStatement.executeUpdate();
		
	}
}
