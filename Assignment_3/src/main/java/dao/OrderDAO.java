package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.Order;
/**
 * OrderDAO class which implements the queries in order to perform insert, delete, update, findAll, findByClientId, findId
 * operations on the order table from the database
 */
public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO bill (id,client_id,comments,total)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM bill where client_id= ?";
	private final static String findByIdStatementString = "SELECT * FROM bill where id= ?";
	private final static String findAllStatementString = "SELECT * FROM bill ";
	private static final String updateStatementString = "UPDATE bill SET client_id=?, comments=?, total=? WHERE id=?";


	private final static String deleteStatementString = "DELETE FROM bill where  id=?";
	
	public static Order findId(int orderId) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findByIdStatementString);
			findStatement.setInt(1, orderId); // search for the name of a product
			rs = findStatement.executeQuery();
			rs.next();
			
			int id=rs.getInt("id");
			int clientId = rs.getInt("client_id");
			String comments = rs.getString("comments");
			double total=rs.getDouble(4); // 4 is the column index
			toReturn = new Order(id, clientId, comments, total);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByClientId " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static Order findByClientId(int searchedClientId) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, searchedClientId); // search for the name of a product
			rs = findStatement.executeQuery();
			rs.next();
			
			int id=rs.getInt("id");
			int clientId = rs.getInt("client_id");
			String comments = rs.getString("comments");
			double total=rs.getDouble(4); // 4 is the column index
			toReturn = new Order(id, clientId, comments, total);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByClientId " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getId());
			insertStatement.setInt(2, order.getClientId());
			insertStatement.setString(3, order.getComments());
			insertStatement.setDouble(4, 0.0);
			insertStatement.executeUpdate();
			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public void delete(int orderId) { // deleting method
		//Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, orderId);
			deleteStatement.executeUpdate();
			
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static ArrayList<Order> findAll() {
		ArrayList<Order> toReturn = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();			
		Statement findStatement = null;;
		ResultSet rs = null;
		try {
			findStatement  = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery(findAllStatementString);
			while(rs.next()) {
				
			int orderId=rs.getInt("id");
			int customerId=rs.getInt("client_id");
			String comments = rs.getString("comments");
			double total=rs.getDouble("total");
			toReturn.add(new Order(orderId, customerId, comments, total));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;		
	}
	
	public static void update(Order order) throws SQLException {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement insertStatement = null;

		insertStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
		insertStatement.setInt(4, order.getId());
		insertStatement.setInt(1, order.getClientId());
		insertStatement.setString(2, order.getComments());
		insertStatement.setDouble(3, order.getTotal());
		insertStatement.executeUpdate();
		
	}
}
