package dao;

//for now here I have findByOrderId, findAll and insert methods
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.OrderDetail;
/**
 * OrderDetailDAO class which implements the queries in order to perform insert, delete, findAll, findByOrderId
 * operations on the order_detail table from the database
 */
public class OrderDetailDAO {

	protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO order_detail (order_id,product_id, quantity, unit_price)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM order_detail where order_id= ?";
	private final static String deleteStatementString = "DELETE FROM order_detail where  order_id=?";
	private final static String findAllStatementString = "SELECT * FROM order_detail ";

	public ArrayList<OrderDetail> findByOrderId(int searchedOrderId) {
		ArrayList<OrderDetail> toReturn = new ArrayList<OrderDetail>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setInt(1, searchedOrderId); // search for the name of a product
			rs = findStatement.executeQuery();
			while (rs.next()) {

				int orderId = rs.getInt("order_id");
				int productId = rs.getInt("product_id");
				int quantity = rs.getInt("quantity");
				double unitPrice = rs.getDouble(4); // 5 is the column index for unitprice
				OrderDetail orderDetail = new OrderDetail(orderId, productId, quantity, unitPrice);
				toReturn.add(orderDetail);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(OrderDetail orderDetail) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;//System.out.println(orderDetail);
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString);//, Statement.RETURN_GENERATED_KEYS
			insertStatement.setInt(1, orderDetail.getOrderId());
			insertStatement.setInt(2, orderDetail.getProductId());
			insertStatement.setInt(3, orderDetail.getQuantity());
			insertStatement.setDouble(4, orderDetail.getUnitPrice());
			
			insertStatement.executeUpdate();
			//ResultSet rs = insertStatement.getGeneratedKeys();
			//if (rs.next()) {
				//insertedId = rs.getInt(1);
			//}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	public static void delete(int orderId) { // deleting method
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		ResultSet rs = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, orderId);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static ArrayList<OrderDetail> findAll() {
		ArrayList<OrderDetail> toReturn = new ArrayList<OrderDetail>();
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement findStatement = null;
		;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findAllStatementString);
			rs = findStatement.executeQuery(findAllStatementString);
			while (rs.next()) {

				int orderId = rs.getInt("order_id");
				int productId = rs.getInt("product_id");
				int quantity = rs.getInt("quantity");
				double unityPrice = rs.getDouble("unit_price");
				toReturn.add(new OrderDetail(orderId, productId, quantity, unityPrice));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDetailDAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
}
