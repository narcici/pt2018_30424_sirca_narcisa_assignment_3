package presentationLayer;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class CustomerView {

	private JFrame frame;
	private JTextField customerId;
	private JTextField customerName;
	private JTextField customerAddress;
	private JTextField customerEmail;
	private JTextField customerPhoneNr;
	public JButton btnInsert;
	public JButton btnUpdate;
	public JButton btnDelete;
	public JButton btnFind;
	public JButton btnListCustomers;
	
	public CustomerView() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 388);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		customerId = new JTextField();
		customerId.setBounds(12, 34, 116, 22);
		frame.getContentPane().add(customerId);
		customerId.setColumns(10);
		
		JLabel lblCustomersId = new JLabel("Customer's id");
		lblCustomersId.setBounds(12, 13, 102, 16);
		frame.getContentPane().add(lblCustomersId);
		
		JLabel lblCustomersName = new JLabel("Customer's name");
		lblCustomersName.setBounds(12, 67, 116, 16);
		frame.getContentPane().add(lblCustomersName);
		
		customerName = new JTextField();
		customerName.setBounds(12, 88, 116, 22);
		frame.getContentPane().add(customerName);
		customerName.setColumns(10);
		
		JLabel lblCustomersAddress = new JLabel("Customer's address");
		lblCustomersAddress.setBounds(12, 123, 126, 16);
		frame.getContentPane().add(lblCustomersAddress);
		
		customerAddress = new JTextField();
		customerAddress.setBounds(12, 141, 116, 22);
		frame.getContentPane().add(customerAddress);
		customerAddress.setColumns(10);
		
		JLabel lblCustomersEmail = new JLabel("Customer's email");
		lblCustomersEmail.setBounds(12, 176, 102, 16);
		frame.getContentPane().add(lblCustomersEmail);
		
		customerEmail = new JTextField();
		customerEmail.setBounds(12, 194, 116, 22);
		frame.getContentPane().add(customerEmail);
		customerEmail.setColumns(10);
		
		JLabel lblCustomersPhoneNumber = new JLabel("Customer's phone number");
		lblCustomersPhoneNumber.setBounds(12, 244, 182, 16);
		frame.getContentPane().add(lblCustomersPhoneNumber);
		
		customerPhoneNr = new JTextField();
		customerPhoneNr.setBounds(12, 267, 116, 22);
		frame.getContentPane().add(customerPhoneNr);
		customerPhoneNr.setColumns(10);
		
		btnInsert = new JButton("INSERT");
		btnInsert.setBounds(229, 33, 143, 25);
		frame.getContentPane().add(btnInsert);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(229, 87, 143, 25);
		frame.getContentPane().add(btnUpdate);
		
		btnDelete = new JButton("DELETE");
		btnDelete.setBounds(229, 151, 143, 25);
		frame.getContentPane().add(btnDelete);
		/*
		btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
		btnFind.setBounds(229, 204, 143, 25);
		frame.getContentPane().add(btnFind);
		*/
		btnListCustomers = new JButton("LIST CUSTOMERS");
		btnListCustomers.setBounds(229, 266, 143, 25);
		frame.getContentPane().add(btnListCustomers);
	}

	public JFrame getFrame() {
		return frame;
	}

	public String getCustomerId() {
		return customerId.getText();
	}

	public String getCustomerName() {
		return customerName.getText();
	}

	public String getCustomerAddress() {
		return customerAddress.getText();
	}

	public String getCustomerEmail() {
		return customerEmail.getText();
	}

	public String getCustomerPhoneNr() {
		return customerPhoneNr.getText();
	}
}
