package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import javax.swing.JButton;

public class OrderView {

	private JFrame frame;
	private JTextField orderId;
	private JTextField customerId;
	private JTextField comments;
	private JTextField total;
	private JTextField productId;
	private JTextField quantity;
	private JTextField orderIdAdd;
	
	public JButton btnAddProduct;
	public JButton btnAddOrder;
	public JButton btnListOrders;
	public JButton btnListProducts_1;
	public JButton btnDeleteOrder;
	

	public JFrame getFrame() {
		return frame;
	}

	public OrderView() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 436);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblOrdersId = new JLabel("Order's id");
		lblOrdersId.setBounds(24, 13, 71, 16);
		frame.getContentPane().add(lblOrdersId);
		
		orderId = new JTextField();
		orderId.setBounds(24, 35, 116, 22);
		frame.getContentPane().add(orderId);
		orderId.setColumns(10);
		
		JLabel lblCustomersId = new JLabel("Customer's id");
		lblCustomersId.setBounds(24, 82, 116, 16);
		frame.getContentPane().add(lblCustomersId);
		
		customerId = new JTextField();
		customerId.setBounds(24, 101, 116, 22);
		frame.getContentPane().add(customerId);
		customerId.setColumns(10);
		
		JLabel lblComments = new JLabel("Comments");
		lblComments.setBounds(24, 136, 82, 16);
		frame.getContentPane().add(lblComments);
		
		comments = new JTextField();
		comments.setBounds(24, 155, 116, 22);
		frame.getContentPane().add(comments);
		comments.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setBounds(24, 190, 56, 16);
		frame.getContentPane().add(lblTotal);
		
		total = new JTextField();
		total.setBounds(24, 218, 116, 22);
		frame.getContentPane().add(total);
		total.setColumns(10);
		
		btnAddOrder = new JButton("Add order");
		btnAddOrder.setBounds(24, 272, 116, 25);
		frame.getContentPane().add(btnAddOrder);
		
		btnAddProduct = new JButton("Add Product into an order");
		btnAddProduct.setBounds(228, 272, 179, 25);
		frame.getContentPane().add(btnAddProduct);
		
		JLabel label = new JLabel("");
		label.setBounds(270, 136, 56, 16);
		frame.getContentPane().add(label);
		
		JLabel lblProductsId = new JLabel("Product's id");
		lblProductsId.setBounds(270, 136, 97, 16);
		frame.getContentPane().add(lblProductsId);
		
		productId = new JTextField();
		productId.setBounds(251, 155, 116, 22);
		frame.getContentPane().add(productId);
		productId.setColumns(10);
		
		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(270, 190, 56, 16);
		frame.getContentPane().add(lblQuantity);
		
		quantity = new JTextField();
		quantity.setBounds(251, 218, 116, 22);
		frame.getContentPane().add(quantity);
		quantity.setColumns(10);
		
		JLabel lblFirstlyCheckIf = new JLabel("Firstly check if the order exists");
		lblFirstlyCheckIf.setBounds(218, 240, 189, 25);
		frame.getContentPane().add(lblFirstlyCheckIf);
		
		JLabel lblOrdersId_1 = new JLabel("Order's id");
		lblOrdersId_1.setBounds(270, 82, 56, 16);
		frame.getContentPane().add(lblOrdersId_1);
		
		orderIdAdd = new JTextField();
		orderIdAdd.setBounds(251, 101, 116, 22);
		frame.getContentPane().add(orderIdAdd);
		orderIdAdd.setColumns(10);
		
		btnListOrders = new JButton("List orders");
		btnListOrders.setBounds(24, 338, 117, 25);
		frame.getContentPane().add(btnListOrders);
		
		btnListProducts_1 = new JButton("List products");
		btnListProducts_1.setBounds(251, 34, 116, 25);
		frame.getContentPane().add(btnListProducts_1);
		
		btnDeleteOrder = new JButton("Delete order");
		btnDeleteOrder.setBounds(259, 338, 108, 25);
		frame.getContentPane().add(btnDeleteOrder);
	}

	public String getOrderId() {
		return orderId.getText(); 
	}

	public String getCustomerId() {
		return customerId.getText(); 
	}

	public String getComments() {
		return comments.getText();
	}

	public void setTotal(double total) {
		this.total.setText(String.valueOf(total));
	}

	public String getProductId() {
		return productId.getText();
	}

	public String getQuantity() {
		return quantity.getText(); 
	}

	public String getOrderIdAdd() {
		return 	orderIdAdd.getText(); 
	}
}
