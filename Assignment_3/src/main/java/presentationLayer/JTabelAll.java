package presentationLayer;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

public class JTabelAll {
	private JFrame frame;
	private JTable table;
	private JScrollPane scrollPane = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
	public JTabelAll() {
		initialize();
	}
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 388);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLayout(new GridLayout(1,1));
		frame.add(scrollPane);

	}	
	// this function returns a JTabelAll instance, having the table set accordingly with its param
	public JTable makeATable(String[][]data, String[] columnNames ) {
		 return  new JTable(data, columnNames);
		
	}
	public JFrame getFrame() {
		return frame;
	}
		public void setTable(JTable table) {
		this.table = table;
	}
}