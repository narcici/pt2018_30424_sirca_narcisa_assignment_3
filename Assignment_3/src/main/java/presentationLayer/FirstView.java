package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

public class FirstView {

	private JFrame frame;
	public  JButton btnProduct;
	public  JButton btnCustomer;
	public  JButton btnListCustomers;
	public JButton btnListProducts;
	public JButton btnPlaceOrder;
	
	public FirstView() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 530, 485);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWharehouseStartingPage = new JLabel("Wharehouse starting page");
		lblWharehouseStartingPage.setBounds(158, 13, 218, 16);
		frame.getContentPane().add(lblWharehouseStartingPage);
		
		btnCustomer = new JButton("Customer");
		btnCustomer.setBounds(67, 95, 97, 25);
		frame.getContentPane().add(btnCustomer);
		
		btnProduct = new JButton("Product");
		btnProduct.setBounds(324, 95, 97, 25);
		frame.getContentPane().add(btnProduct);
		
		btnListCustomers = new JButton("List customers");
		btnListCustomers.setBounds(67, 183, 128, 25);
		frame.getContentPane().add(btnListCustomers);
		
		btnListProducts = new JButton("List products");
		btnListProducts.setBounds(282, 183, 139, 25);
		frame.getContentPane().add(btnListProducts);
		
		btnPlaceOrder = new JButton("Place order");
		btnPlaceOrder.setBounds(182, 321, 140, 25);
		frame.getContentPane().add(btnPlaceOrder);
	}

	public JFrame getFrame() {
		return frame;
	}
}
