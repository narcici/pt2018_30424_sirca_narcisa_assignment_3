package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ProducTtry {

	private JFrame frame;
	private JTextField productId;
	private JTextField productName;
	private JTextField productPrice;
	private JTextField productQuantity;
	public JButton btnInsert; //insert a product
	public JButton btnUpdate;
	public JButton btnDelete;
	public JButton btnListProducts;
	public JButton btnFind;
	public JButton btnOrderByPrice;
	private JButton btnSearchByName;
	private JTextField searchProduct;
	
	public ProducTtry() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		productId = new JTextField();
		productId.setBounds(12, 34, 116, 22);
		frame.getContentPane().add(productId);
		productId.setColumns(10);
		
		JLabel lblCustomersId = new JLabel("Product's id");
		lblCustomersId.setBounds(12, 13, 102, 16);
		frame.getContentPane().add(lblCustomersId);
		
		JLabel lblCustomersName = new JLabel("Product's name");
		lblCustomersName.setBounds(12, 67, 116, 16);
		frame.getContentPane().add(lblCustomersName);
		
		productName = new JTextField();
		productName.setBounds(12, 88, 116, 22);
		frame.getContentPane().add(productName);
		productName.setColumns(10);
		
		JLabel lblCustomersAddress = new JLabel("Product's price");
		lblCustomersAddress.setBounds(12, 123, 126, 16);
		frame.getContentPane().add(lblCustomersAddress);
		
		productPrice = new JTextField();
		productPrice.setBounds(12, 141, 116, 22);
		frame.getContentPane().add(productPrice);
		productPrice.setColumns(10);
		
		JLabel lblCustomersEmail = new JLabel("Product's quantity");
		lblCustomersEmail.setBounds(12, 176, 102, 16);
		frame.getContentPane().add(lblCustomersEmail);
		
		productQuantity = new JTextField();
		productQuantity.setBounds(12, 194, 116, 22);
		frame.getContentPane().add(productQuantity);
		productQuantity.setColumns(10);
		
		btnInsert = new JButton("INSERT");
		btnInsert.setBounds(229, 33, 143, 25);
		frame.getContentPane().add(btnInsert);
		
		btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(229, 87, 143, 25);
		frame.getContentPane().add(btnUpdate);
		
		btnDelete = new JButton("DELETE");
		btnDelete.setBounds(229, 151, 143, 25);
		frame.getContentPane().add(btnDelete);
		
		btnFind = new JButton("FIND");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnFind.setBounds(229, 204, 143, 25);
		frame.getContentPane().add(btnFind);
		
		btnListProducts = new JButton("LIST PRODUCTS");
		btnListProducts.setBounds(229, 264, 143, 25);
		frame.getContentPane().add(btnListProducts);
		
		btnOrderByPrice = new JButton("ORDER BY PRICE");
		btnOrderByPrice.setBounds(229, 316, 143, 25);
		frame.getContentPane().add(btnOrderByPrice);
		
		btnSearchByName = new JButton("SEARCH BY NAME");
		btnSearchByName.setBounds(12, 316, 153, 25);
		frame.getContentPane().add(btnSearchByName);
		
		JLabel lblProductByName = new JLabel("Product by name");
		lblProductByName.setBounds(12, 231, 116, 16);
		frame.getContentPane().add(lblProductByName);
		
		searchProduct = new JTextField();
		searchProduct.setBounds(12, 265, 116, 22);
		frame.getContentPane().add(searchProduct);
		searchProduct.setColumns(10);
	}

	public JFrame getFrame() {
		return frame;
	}

	public String getProductId() {
		return productId.getText();
	}

	public String getProductName() {
		return productName.getText();
	}

	public String getProductPrice() {
		return productPrice.getText();
	}

	public String getProductQuantity() {
		return productQuantity.getText();
	}

	public void setSearchProduct(String searchProduct) {
		this.searchProduct.setText(searchProduct);
	}
}
