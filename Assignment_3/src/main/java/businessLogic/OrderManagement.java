package businessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import dao.OrderDAO;
import dao.OrderDetailDAO;
import dao.ProductDAO;
import model.Order;
import model.OrderDetail;
import model.Product;
import presentationLayer.JTabelAll;
import presentationLayer.OrderView;

public class OrderManagement {
		private OrderDAO orderDAO= new OrderDAO();
		private OrderDetailDAO orderdetailDAO =new OrderDetailDAO();
		private OrderView orderView; //= new OrderView();
		private JTabelAll orderDetailTabel= new JTabelAll();
		private JTabelAll productTabel= new JTabelAll();

	public OrderManagement(OrderView orderView) {
		this.orderView= orderView;
		insertOrder();
		insertOrderDetail();
		findAllOrders();
		findAllProducts();
		deleteOrder();
		
		}
	public void insertOrder() {
		orderView.btnAddOrder.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(orderView.getOrderId());
				int customerId= Integer.parseInt(orderView.getCustomerId());
				String comments =orderView.getComments();
				orderDAO.insert(new Order(id,customerId,comments,0.0));
				}
			});
	}
	public void insertOrderDetail() { // add a new product to an existing order
		orderView.btnAddProduct.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int orderId= Integer.parseInt(orderView.getOrderIdAdd());
				int productId= Integer.parseInt(orderView.getProductId());
				Product p= ProductDAO.findById(productId); // I don't insert the price here, the price is already belonging to the product
				int quantity= Integer.parseInt(orderView.getQuantity()); // quantity I want to add
				if( p == null) 
				JOptionPane.showMessageDialog(orderView.getFrame(), "Please, choose an existing product!", "Warning", JOptionPane.WARNING_MESSAGE);
				else
					if(quantity > ProductDAO.findById(productId).getQuantity()) // CHECK IF THERE EXISTS SO MANY PRODUCTS
					JOptionPane.showMessageDialog(orderView.getFrame(), "Not enough products. Please choose a smaller quantity!", "Warning", JOptionPane.WARNING_MESSAGE);
				else {						
						if (OrderDAO.findId(orderId) == null)
							JOptionPane.showMessageDialog(orderView.getFrame(), "Please, choose an existing order!", "Warning", JOptionPane.WARNING_MESSAGE);
						else {
							try {
								double unityPrice =p.getPrice();
								int newQ= p.getQuantity() - quantity; // DECREASE THE QUANTITY
								OrderDetail orderDetail=new OrderDetail(orderId, productId, quantity, unityPrice);
								OrderDetailDAO.insert(orderDetail);
								ProductDAO.update(p.getId(), p.getName(), p.getPrice(),  newQ);
								setTotal(orderId);
								} catch (SQLException e1) {
									e1.printStackTrace();
								} 
							}
						}
				}					
			});
	}
	public void findAllOrders() { // list the orders
		orderView.btnListOrders.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				JTable ot= orderDetailTabel.makeATable(TablePrepare.createStringMatrix(OrderDAO.findAll()), TablePrepare.makeTableHeader(OrderDAO.findAll().get(0)));
				orderDetailTabel.setTable(ot);
				JFrame frame =new JFrame();
				frame.setSize(300, 300);
				frame.add(ot);
				frame.setVisible(true);	
				JTable ct=orderDetailTabel.makeATable(TablePrepare.createStringMatrix(OrderDetailDAO.findAll()), TablePrepare.makeTableHeader(OrderDetailDAO.findAll().get(0)));
				orderDetailTabel.setTable(ct);
				//tabelClients.getFrame().setVisible(true);
				JFrame frame2 =new JFrame();
				frame2.setSize(400, 400);
				frame2.add(ct);
				frame2.setVisible(true);	}
		});
   }
	public void findAllProducts() { // show the products here
		orderView.btnListProducts_1.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				JTable pt=productTabel.makeATable(TablePrepare.createStringMatrix(ProductDAO.findAll()), TablePrepare.makeTableHeader(ProductDAO.findAll().get(0)));
				productTabel.setTable(pt);
				//productsTabel.getFrame().setVisible(true);
				JFrame frame =new JFrame();
				frame.setSize(600, 600);
				frame.add(pt);
				frame.setVisible(true);			}
		});
	}
	public void deleteOrder() {
		orderView.btnDeleteOrder.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(orderView.getOrderId());
				orderDAO.delete(id);
				OrderDetailDAO.delete(id); // I delete the orderDetails that belong to this order
				}
			});
	}
	private void setTotal(int orderId) throws SQLException {
		ArrayList<OrderDetail> listOrderDetails = orderdetailDAO.findByOrderId(orderId);
		double totalSum=0;
		for(OrderDetail od:listOrderDetails)
			totalSum= totalSum + od.getUnitPrice()* od.getQuantity();
		Order order=OrderDAO.findId(orderId);
		order.setTotal(totalSum);
		OrderDAO.update(order); //here I ret the total of this order
		
	}
}
