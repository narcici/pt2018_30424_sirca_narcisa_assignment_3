package businessLogic;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JTable;

// here it's the connection bt ProiectDAO and ProductView
import dao.ProductDAO;
import model.Product;
import presentationLayer.JTabelAll;
import presentationLayer.ProductView;

public class ProductManagement {
	private ProductDAO productDAO= new ProductDAO();
	private ProductView productView; // = new ProductView();
	private JTabelAll productsTabel = new JTabelAll();
	
	public ProductManagement(ProductView productView) {
			this.productView= productView;
			insertProduct();
			findProductByID();
			findAllProducts();
			updateProduct();
			deleteProduct();
			productView.getFrame().setVisible(true);

		}
	public void insertProduct() {
		//insert a new product
				productView.btnInsert.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						int id= Integer.parseInt(productView.getProductId());
						String name= productView.getProductName();
						double price = Double.parseDouble(productView.getProductPrice());
						int quantity = Integer.parseInt(productView.getProductQuantity());
						ProductDAO.insert(new Product(id,name,price,quantity));
						}
					}); 
	}
	public void findProductByID() {
		productView.btnFind.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(productView.getProductId());
				ProductDAO.findById(id);               // vezi aici ce faci cu un produc gasit
				}
			}); 
	}
	public void searchByName(String s) {
		productView.btnSearchByName.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				String s= productView.getProductName();
				Product found=ProductDAO.findByName(s); //checked this one 
				
				System.out.print(found.toString());

				productView.setSearchProduct(found.toString2());
				}
			}); 
	}
	public void findAllProducts() {
		productView.btnListProducts.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				JTable pt=productsTabel.makeATable(TablePrepare.createStringMatrix(ProductDAO.findAll()), TablePrepare.makeTableHeader(ProductDAO.findAll().get(0)));
				productsTabel.setTable(pt);
				//productsTabel.getFrame().setVisible(true);
				JFrame frame =new JFrame();
				frame.setSize(600, 600);
				frame.add(pt);
				frame.setVisible(true);
			}
			}); 
	}
	
	public void orderByPrice() {
		productView.btnOrderByPrice.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				JTable pt=productsTabel.makeATable(TablePrepare.createStringMatrix(ProductDAO.orderByPrice()), TablePrepare.makeTableHeader(ProductDAO.orderByPrice().get(0)));
				productsTabel.setTable(pt);
				productsTabel.getFrame().setVisible(true);
				JFrame frame =new JFrame();
				frame.setSize(600, 600);
				frame.add(pt);
				frame.setVisible(true);
			}
			}); 
	}
	
	public void updateProduct() {
		productView.btnUpdate.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(productView.getProductId());
				String name= productView.getProductName();
				double price = Double.parseDouble(productView.getProductPrice());
				int quantity = Integer.parseInt(productView.getProductQuantity());
				try {
					ProductDAO.update(id,name,price,quantity);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}); 
	}
	public void deleteProduct() {
		productView.btnDelete.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(productView.getProductId());
				productDAO.delete(id);
				//System.out.println(id);
				}
			}); 
	}

}
