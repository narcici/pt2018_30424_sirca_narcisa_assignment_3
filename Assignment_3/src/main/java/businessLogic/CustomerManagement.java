package businessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JTable;

import dao.CustomerDAO;
import model.Customer;
import presentationLayer.CustomerView;
import presentationLayer.JTabelAll;
public class CustomerManagement {

	private CustomerDAO customerDAO = new CustomerDAO();
	private CustomerView customerView; // = new CustomerView();
	private JTabelAll tabelClients =new JTabelAll(); 
	
	public CustomerManagement(CustomerView customerView) { // CustomerView customerView
			this.customerView= customerView;
			insertCustomer();
			findAllCustomers();
			//findCustomerByID();
			updateCustomer();
			deleteCustomer();
			customerView.getFrame().setVisible(true);

		}
	/*
	 * @return void,this method has an ActionListener for the btnInsert of the CustomerView view and when this button is pressed
	 * the data from the  fields are collected, a customer is created having them as parameters and by calling insert(Customer) of the 
	 * dao.CustomerDAO class, the customer is inserted in the database
	 */
	public void insertCustomer() {
		customerView.btnInsert.addActionListener(new ActionListener() { 
					public void actionPerformed(ActionEvent e) {
						int id= Integer.parseInt(customerView.getCustomerId());
						String name= customerView.getCustomerName();
						String address=customerView.getCustomerAddress();
						String email= customerView.getCustomerEmail();
						String phoneNr= customerView.getCustomerPhoneNr();
						CustomerDAO.insert(new Customer(id, name, address, email, phoneNr));
						}
					}); 
	}
	/*
	 * @return void,this method has an ActionListener for the btnFind of the CustomerView view and when this button is pressed
	 * the id from the customerId field is collected and by calling findById(int) of the dao.CustomerDAO class, the customer is 
	 * searched in the database and it is returned, I wanted to display it somewhere...
	
	public void findCustomerByID() {
		customerView.btnFind.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(customerView.getCustomerId());
				CustomerDAO.findById(id);               // vezi aici ce faci cu un produc gasit
				}
			}); 
	}
	 */
	
	/*
	 * @return void, this method has an ActionListener for the btnListCustomers of the CustomerView view and when this button is pressed
	 * all the customers from the database are listed in a new  table by calling JTabelAll.makeATable(String[][], String[]) 
	 */
	public void findAllCustomers() {
		customerView.btnListCustomers.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {		
				JTable ct=tabelClients.makeATable(TablePrepare.createStringMatrix(CustomerDAO.findAll()), TablePrepare.makeTableHeader(CustomerDAO.findAll().get(0)));
				tabelClients.setTable(ct);
				JFrame frame =new JFrame();
				frame.setSize(600, 600);
				frame.add(ct);
				frame.setVisible(true);
			}
			}); 
	}
	/*
	 * @return void,this method has an ActionListener for the btnUpdate of the CustomerView view and when this button is pressed
	 * the data from the  fields are collected and  by calling update(int, int, int, double) 
	 * of the dao.CustomerDAO class, the customer is updated with the new values in the database 
	 */
	public void updateCustomer() {
		customerView.btnUpdate.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(customerView.getCustomerId());
				String name= customerView.getCustomerName();
				String address=customerView.getCustomerAddress();
				String email= customerView.getCustomerEmail();
				String phoneNr= customerView.getCustomerPhoneNr();
				try {
					CustomerDAO.update(id,name,address,email,phoneNr);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
			}); 
	}
	/*
	 * @return void,this method has an ActionListener for the btnDelete of the CustomerView view and when this button is pressed
	 * the  id from the customerId field is collected and by calling delete(int)
	 * of the dao.CustomerDAO class, the customer is updated with the new values in the database 
	 */
	public void deleteCustomer() {
		customerView.btnDelete.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				int id= Integer.parseInt(customerView.getCustomerId());
				customerDAO.delete(id);
				}
			}); 
	}

}
