package businessLogic;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class TablePrepare {
	
	public static String[][] createStringMatrix(ArrayList listOfObjects) {
		String[][] matrix= new String[listOfObjects.size()][listOfObjects.get(0).getClass().getDeclaredFields().length];
		int i=0;
		int j=0;
		for(Object object:listOfObjects) {
			j=0;
		for (Field field : object.getClass().getDeclaredFields()) {
			
			field.setAccessible(true); 
			try {
				matrix[i][j] = field.get(object).toString();

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			j++;
		}
		i++;
		}
		return matrix;
	}
	
	public static String[] makeTableHeader(Object object) {
		String[] header= new String[object.getClass().getDeclaredFields().length];
		int i=0;
		for(Field s:object.getClass().getDeclaredFields()) {
			header[i]= s.getName();
			i++;
		}
		return header;
	}
}