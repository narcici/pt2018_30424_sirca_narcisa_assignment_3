package businessLogic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTable;

import dao.*;
import presentationLayer.CustomerView;
import presentationLayer.FirstView;
import presentationLayer.JTabelAll;
import presentationLayer.OrderView;
import presentationLayer.ProductView;

public class Controller {
	protected FirstView firstView = new FirstView();
	protected OrderManagement om;// = new OrderManagement();
	protected CustomerManagement cm; // = new CustomerManagement();
	protected ProductManagement pm; // = new ProductManagement();
	protected JTabelAll tabelClients = new JTabelAll();
	protected JTabelAll productsTabel = new JTabelAll();

	/*
	 * @return a Controller object because it it a constructor, this method calls
	 * for all the methods of this class
	 */
	public Controller() {
		firstView.getFrame().setVisible(true);
		customerManagement();
		productManagement();
		placeOrder();
		findAllCustomers();
		findAllProducts();
	}

	/*
	 * @return void, method has an ActionListener for the btnCustomer of the
	 * firstView of this class in case the button was pressed, here it is launched a
	 * CustomerView and a CustomerManagement so the Customer table can be edited
	 */
	public void customerManagement() {
		firstView.btnCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerView cv = new CustomerView();
				cm = new CustomerManagement(cv);
			}
		});
	}

	/*
	 * @return void, method has an ActionListener for the btnProduct of the
	 * firstView of this class in case the button was pressed, here it is launched a
	 * ProductView and a ProductManagement so the Product table can be edited
	 */ 
	public void productManagement() {
		firstView.btnProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductView pv = new ProductView();
				pv.getFrame().setVisible(true);
				pm = new ProductManagement(pv);
			}
		});
	}

	/*
	 * @return void, method has an ActionListener for the btnPlaceOrder of the
	 * firstView of this class in case the button was pressed, here it is launched a
	 * OrderView and a OrderManagement so the Order and OrderDetail tables can be edited
	 */
	public void placeOrder() {
		firstView.btnPlaceOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderView ov = new OrderView();
				ov.getFrame().setVisible(true);
				om = new OrderManagement(ov);
			}
		});
	}

/*
 * @return void this method has an ActionListener for the btnListCustomers and when this button is pressed
 * a frame containing a table with all the customers from the database is launched
 */
	public void findAllCustomers() {
		firstView.btnListCustomers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTable ct = tabelClients.makeATable(TablePrepare.createStringMatrix(CustomerDAO.findAll()),
						TablePrepare.makeTableHeader(CustomerDAO.findAll().get(0)));
				tabelClients.setTable(ct);
				// tabelClients.getFrame().setVisible(true);
				JFrame frame = new JFrame();
				frame.setSize(600, 600);
				frame.add(ct);
				frame.setVisible(true);
			}
		});
	}

	/*
	 * @return void this method has an ActionListener for the btnListProducts and when this button is pressed
	 * a frame containing a table with all the products from the database is launched
	 */
	public void findAllProducts() {
		firstView.btnListProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTable pt = productsTabel.makeATable(TablePrepare.createStringMatrix(ProductDAO.findAll()),
						TablePrepare.makeTableHeader(ProductDAO.findAll().get(0)));
				productsTabel.setTable(pt);
				// productsTabel.getFrame().setVisible(true);
				JFrame frame = new JFrame();
				frame.setSize(600, 400);
				frame.add(pt);
				frame.setVisible(true);
			}
		});
	}
}
